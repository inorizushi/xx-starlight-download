# CHANGELOG #

# Theme Package #

#### PLEASE NOTE THAT SNCHARACTERS MUST BE MOVED TO THE APPEARANCE FOLDER IF USING STEPMANIA 5.3 OUTFOX ####

## V1.4.# ##

### V1.4.1 2021/08/05 ###
 * Improper OGG audio sample rates fixed. (Fixes crashes on Outfox 4.9.9)
 * Enlarge Song Options menu
 * Remove + symbols from Hidden/Sudden player options
 * Fix Score dates not updating on Default and A Select Music screens

### V1.4.0 2021/03/16 ###
 * Simply Love Player Options menu has been removed.
 * Mini has been added to the Player Options menu.
 * Add InfoPanel hotkeys to SSM footer.
 * Update game icon font to include pad left + right
 * Add InfoPanel to 4th Mix and Jukebox wheels
 * Save InfoPanel visibility to ProfilePrefs.
 * Option items in PlayerOptions now animate when changing.

--------------------------------------------------------------

## V1.3.# ##

### V1.3.0 2020/08/20 ###
 * Risky Gauge now displays correctly on 4:3 and now uses the danger lifemeter.
 * ScrollBar no longer displays on Course Select.
 * Holding start to choose player modifiers now displays a slimmed down version of Simply Love's Options screen with respect for ITG/SMX cab players.
 * Jacket loading code has been improved significantly and should reduce memory usage on Music Select Screens. (4th mix still laggy)

--------------------------------------------------------------

## V1.2.# ##

### V1.2.1 2020/05/30 ###
 * Grades on ScreenEvaluation now reflect the "ConvertScoresAndGrades" setting.
 * Score Difference on ScreenEvaluation is now correct.
 * FullCombo Splash now displays correctly when using Center 1 Player
 * Cut-ins now longer clip into the play area when using Center 1 Player
 * Show HowToPlay screen now defaults to never show. It can be re-enabled in Settings>Theme Options or Customization>Gameplay Settings

### V1.2.0 2020/05/05 ###
 * Theme now displays properly in 4:3 Aspect Ratio
 * Improve quality of the Default Menu Background
 * Fix combo disappearing under certain circumstances

--------------------------------------------------------------

## V1.1.# ##
#### PLEASE NOTE THAT SNCHARACTERS MUST BE MOVED TO THE APPEARANCE FOLDER IF USING STEPMANIA 5.3 OUTFOX ####

### 2020/04/19 ###
* Fix Groove Radar size on 5thMIX - X Music Select
* Fix transition for System Options > Customize Menu/Theme Options (No longer opens the MenuBG menu instantly)

### 2020/04/18 ###
* Theme now respects StepMania's FailOffOnBeginner/FailOffOnFirstEasy settings.
* Various bug fixes

### 2020/03/26 ###
* Character Cut-in's no longer show on Doubles
* Full Combo splash is now fixed on Doubles
* Fix Absolute Path for 5.3
* Add Music redirect fix (Previously if a dev used different music than another user, the music would not be updated for the other user.)
* Fix game not resetting to regular play when exiting Course Mode
* Extend maxwidth of title on Wheel Music Wheel

### 2020/03/25 ###
* Fix SongAttributes not loading group.ini if group folder is loaded from AdditionalFolders
* Remove custom code for playing song previews (was causing problems with some things)
* Fix Music preview stopping when exiting player options popup
* Allow course mode to appear when set to event mode

### 2020/02/23 ###
* Various fixes for SM5.3
#### PLEASE NOTE THAT SNCHARACTERS MUST BE MOVED TO THE APPEARANCE FOLDER IF USING STEPMANIA 5.3 OUTFOX ####
* Re-add keybinds to open SortList with PadLeft/Right

### 2020/02/16 ###
* New Player Stats handling.
* Shrink Judgment and Combo elements on Gameplay.
* Add GameOver overlays when a player during Versus.
* ScreenAttract sounds again
* Various Optimizations

--------------------------------------------------------------

## V1.0.2 ##

### 2020/01/01 ###
* Fix Roulette Wheel Item displaying incorrectly.
* Replace Character images on Select Style.
* Fix ScreenMovie Attract Sound not following SM's Attract Mode settings.

### 2019/12/17 ###
* Fix Good and Miss counters on SelectMusic reporting incorrectly

### 2019/11/30 ###
* Fix ProfilePrefs Errors & Missing Elements on Select Music
* Fix Menu Timer overlaying 2P difficulty selection
* Fix placement of Long/Marathon overlay on SelectMusic
* Remove Difficulty Row from Player Options

### 2019/11/29 ###
* Further fix Extra Stage errors and hanging
* Fix placement errors of score and grades on DDR A style Music Wheel
* Add Difficulty Row in Player Options

### 2019/11/26 ###
* Fix issue where a portion of the scroll bar appears over the MusicWheel
* Fix issue where top and bottom of the banner on SN- X MusicWheel can get cut off
* Fix Artifacts on Combo Numbers
* Fix Theme stalling before extra stage gameplay

### 2019/11/25 ###
* Add USB Profile support to ScreenSelectProfile
* Add handling to ScreenSelectProfile for 2 players. Screen will not transition until both players have confirmed their profiles. 
* Add "Card In" during Attract Screens
* Fix wheel movement on DDR A style Music Select (No longer skips groups when at start or end of the song list)
* Add Scroll Bars to DDR A and SN - X Music Wheels
* Add more details to DDR A Wheel
* Translate Sort Jackets
* Add Group Banner handling to Jukebox and SN - X Wheels
* Fix background on ScreenMapControllers
* Add in some missing characters for song titles/artists

