# This version of XX -starlight- is no longer maintained. Please go [here](https://github.com/MidflightDigital/XX--STARLiGHT--twopointzero) for the most recent versions. #

![alt text][logo]

[logo]:https://bitbucket.org/inorizushi/xx-starlight-download/raw/dbf6e4dcc4c6f03820068d2a65a83f7549bc99c7/xxlogo.png "DDR XX -starlight- logo"

# README #

This repository acts as the download source for the DDR XX -starlight- theme for StepMania 5.0.12, 5.1-new, with support for 5.3 Outfox.

## Where's the Download? ##

* Click on the [Downloads](https://bitbucket.org/inorizushi/xx-starlight-download/downloads/) options in the sidebar and download the latest dated .zip file; or from the [Google Drive Mirror](https://drive.google.com/open?id=15WbDBnP2hOyiRIV6rX_DGSZNkhEYki9G). Always download the files with the highest version number.
* You DO NOT need to download and apply the hotfix if you're downloading the theme package if the upload dates of the theme package and hotfix are similar.

## Installation Instructions ##

### Theme Package: ###
* Delete any previous installations of the theme before updating!
* Open the XX -starlight- .zip
* Extract the folders in the XX -starlight- folder into the root of your StepMania directory
* Extract the "group.ini" and "jacket.png" files into your DDR XX song pack folder.

### Hotfix: ###
* Open the XX -starlight- hotfix .zip
* Extract the contents of the XX -starlight- hotfix folder into your DDR XX -starlight- theme folder.
* Overwrite all files.

## Extras ##
* DancerVideos can be downloaded from here (1GB): https://mega.nz/#!7tpW2JxT!183BKx6l6W-ppFqKngFfQtwXVtaU17wU_-MDkWUVJFI
* Place the video files inside the "DancerVideos" folder of each corresponding character's folder inside "SNCharacters"


=========================================================================

# Copyright Info #

We are in no way affiliated with Konami. Any references made to the DanceDanceRevolution franchise is strictly for continuity purposes.

Do not use any graphical elements from this project without explicit permission from the Graphics Team.

You are allowed to borrow code/metrics if you ask the Metrics/Lua team or if credit is given. Our team has spent many months developing this theme and coming up with newer implementations for several elements. Please respect our work, it'll only take a couple of keystrokes.

You are allowed to edit this theme for PERSONAL USE ONLY.

You are allowed to run this theme in a public setting only with permission from the DDR XX team.


=========================================================================

# Contributors #
## PROJECT MANAGERS ##

HypnoticMarten77

NewbStepper/hnkul702

## STARLiGHT CONCEPT + DESIGN ##

silverdragon754

## GRAPHICS + DESIGN ##

silverdragon754

Inorizushi

black4ever

KowalskiPenguin10897

DDRDAIKENKAI

riskofsoundingnerdy/leeium

HypnoticMarten77

Haley Halcyon

## CODING + PROGRAMMING ##

Inorizushi

tertu

### Special Thanks to: ###

dbk2

Jousway

kenP

razorblade

leadbman

Kyzentun

ZTS

The StepMania Team

Konami

## STEP ARTISTS ##

HypnoticMarten77

NewbStepper/hnkul702

DDRDAIKENKAI

RIME

KexMiX

PandemoniumX

tak


## SOUND DESIGN ##

funkyzukin/MiDO

riskofsoundingnerdy/leeium

aiden9030/saiiko2

Dreamland200/tykoneko

Dynamite Grizzly

xRGTMx/Drisello

Quickman

Sigrev2

djVERTICAI

Haley Halcyon


